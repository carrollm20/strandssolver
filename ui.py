import os
import re
import time
import problem
import word_bank
from termcolor import colored
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import json

class OfflineUserInterface:
    def __init__(self, file="Strands_03_04_2024.txt", heuristic=None):
        self.hint, self.board, self.visibleBoard, self.answers = self.parse_puzzle_file(file)
        self.hint = self.process_hint(self.hint)
        self.heuristic = heuristic
        self.numFound = 0
        self.colorIndex = 0
        self.guesses = 0
        bank = word_bank.WordBank("large_common_words.json")
        self.wordBank = bank.getWordBank()
        for i, word in enumerate(self.answers):
            if i > 0 and not self.is_word_or_singular_in_bank(word):
                bank.add_word(word)
        self.wordBank = self.remove_impossible_words(bank.getWordBank())

    def is_word_or_singular_in_bank(self, word):
        if word in self.wordBank:
            return True

        if word.endswith('s') and word[:-1] in self.wordBank:
            return True

        if word.endswith('es') and word[:-2] in self.wordBank:
            return True
        return False

    def process_hint(self, hint_text):
        no_punctuation = re.sub(r'[^\w\s]', '', hint_text)
        processed_hint = re.sub(r'\s+', '_', no_punctuation)
        return processed_hint

    def remove_impossible_words(self, word_bank):
        available_letters = [letter for row in self.board for letter in row if letter.isalpha()]
        letter_inventory = {}
        for letter in available_letters:
            if letter in letter_inventory:
                letter_inventory[letter] += 1
            else:
                letter_inventory[letter] = 1
        possible_words = []
        for word in word_bank:
            temp_inventory = letter_inventory.copy()
            can_make_word = True
            for letter in word:
                if letter in temp_inventory and temp_inventory[letter] > 0:
                    temp_inventory[letter] -= 1
                else:
                    can_make_word = False
                    break
            if can_make_word:
                possible_words.append(word)
        return possible_words

    def parse_puzzle_file(self, filename):
        with open(filename, 'r') as file:
            content = file.read()

        sections = content.split('\n\n')

        hint = sections[0].replace('HINT:\n', '').strip()
        board = [list(line.lower()) for line in sections[1].replace('BOARD:\n', '').split('\n') if line]
        visibleBoard = [[(char, "black") for char in list(line.lower())] for line in sections[1].replace('BOARD:\n', '').split('\n') if line]
        answers = [answer.lower() for answer in sections[2].replace('ANSWERS:\n', '').split('\n') if answer]

        return hint, board, visibleBoard, answers

    def findWordsFrom(self, state, exist=True):
        game = problem.Game(
            start=state,
            board=self.board,
            costFn=lambda x: 1,
            hint=self.hint,
            answers=self.answers,
            wordBank=self.wordBank,
            guesses=self.guesses
        )
        actions = problem.aStarSearch(game, self.wordBank, exist, heuristic=self.heuristic)
        self.guesses = game.guesses
        if actions == []:
            return None
        start = state
        dx = state[0]
        dy = state[1]
        word = "" + self.board[state[1]][state[0]]
        self.board[state[1]][state[0]] = f'{self.numFound}0'
        for i, action in enumerate(actions):
            state = (state[0] + action[0], state[1] + action[1])
            word += self.board[state[1]][state[0]]
            self.board[state[1]][state[0]] = f'{self.numFound}{i+1}'
        dx -= state[0]
        dy -= state[1]
        self.setColors(start, actions, abs(dx) == 6 or abs(dy) == 8)
        self.printBoard()
        self.wordBank = self.remove_impossible_words(self.wordBank)
        print(f"Guesses used: {self.guesses}")
        self.numFound += 1
        return word

    def setColors(self, start, actions, spanagram):
        colors = [
            'red',
            'green',
            'blue',
            'magenta',
            'cyan',
            'light_red',
            'light_green',
            'light_blue',
            'light_magenta',
            'light_cyan'
        ]
        color = 'yellow'
        if not spanagram:
            color = colors[self.colorIndex]
            self.colorIndex += 1

        state = start
        self.visibleBoard[state[1]][state[0]] = (self.visibleBoard[state[1]][state[0]][0], color)
        for action in actions:
            state = (state[0] + action[0], state[1] + action[1])
            self.visibleBoard[state[1]][state[0]] = (self.visibleBoard[state[1]][state[0]][0], color)

    def printBoard(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        for row in self.visibleBoard:
            formatted_row = ''
            for char, color in row:
                spaced_char = f"{char:{3}}"
                if color is not None:
                    formatted_row += colored(spaced_char, color)
                else:
                    formatted_row += spaced_char
            print(formatted_row)

class OnlineUserInterface:
    def __init__(self, url="https://www.nytimes.com/games/strands", heuristic=None):
        service = ChromeService(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.driver.get(url)

        wait = WebDriverWait(self.driver, 10)
        play_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'Feo8La_playButton')))
        play_button.click()

        close_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.ygtU9G_closeX.cRBDMG_closeX')))
        close_button.click()

        hint_element = wait.until(EC.visibility_of_element_located((By.CLASS_NAME, 'umfyna_clue')))
        self.hint = self.process_hint(hint_element.text)

        self.guesses = 0
        self.numFound = 0
        self.heuristic = heuristic
        self.board = [['0' for x in range(6)] for y in range(8)]
        for i in range(48):
            try:
                # Wait for each button to be present in the DOM and be visible
                letter = wait.until(EC.visibility_of_element_located((By.ID, f"button-{i}")))
                self.board[i // 6][i % 6] = letter.text.lower()
            except Exception as e:
                print(f"Could not find the button with ID button-{i}: {str(e)}")
        self.bank = word_bank.WordBank("large_common_words.json")
        self.wordBank = self.remove_impossible_words(self.bank.getWordBank())
        
        with open("removed.json", 'r') as file:
            self.removed = list(json.load(file))
        with open("real_words.json", 'r') as file:
            self.real_words = list(json.load(file))


    def process_hint(self, hint_text):
        no_punctuation = re.sub(r'[^\w\s]', '', hint_text)
        processed_hint = re.sub(r'\s+', '_', no_punctuation)
        return processed_hint

    def remove_impossible_words(self, wordBank):
        available_letters = [letter for row in self.board for letter in row if letter.isalpha()]
        letter_inventory = {}
        for letter in available_letters:
            if letter in letter_inventory:
                letter_inventory[letter] += 1
            else:
                letter_inventory[letter] = 1
        possible_words = []
        for word in wordBank:
            temp_inventory = letter_inventory.copy()
            can_make_word = True
            for letter in word:
                if letter in temp_inventory and temp_inventory[letter] > 0:
                    temp_inventory[letter] -= 1
                else:
                    can_make_word = False
                    break
            if can_make_word:
                possible_words.append(word)
        return possible_words

    def findWordsFrom(self, state, exist=True):
        game = problem.Game(
            start=state,
            board=self.board,
            costFn=lambda x: 1,
            hint=self.hint,
            online=True,
            onlineUI=self,
            wordBank=self.wordBank,
            exist=exist,
            guesses=self.guesses
        )
        actions = problem.aStarSearch(game, self.wordBank, exist, heuristic=self.heuristic)
        self.guesses = game.guesses
        if actions == []:
            return None
        word = "" + self.board[state[1]][state[0]]
        self.board[state[1]][state[0]] = f'{self.numFound}0'
        for i, action in enumerate(actions):
            state = (state[0] + action[0], state[1] + action[1])
            word += self.board[state[1]][state[0]]
            self.board[state[1]][state[0]] = f'{self.numFound}{i+1}'
        self.wordBank = self.remove_impossible_words(self.wordBank)
        print(f"Guesses used: {self.guesses}")
        self.numFound += 1
        return word

    def checkAnswer(self, state, actions, word):
        # if(word in ["sponge","razor","shampoo","conditioner","washcloth","shower","soap"]):
        #     self.real_words.append(word)
        #     with open('real_words.json', 'w') as f:
        #         json.dump(list(self.real_words), f)
        #     return False
        # if(word in self.removed):
        #     return False
        order = [state[0] + (state[1] * 6)]
        for action in actions:
            state = (state[0] + action[0], state[1] + action[1])
            order.append(state[0] + (state[1] * 6))
        order.append(state[0] + (state[1] * 6))

        wait = WebDriverWait(self.driver, 10)
        for button_index in order:
            try:
                button = wait.until(EC.visibility_of_element_located((By.ID, f"button-{button_index}")))
                button.click()
            except Exception as e:
                print(f"Could not find or click the button with ID button-{button_index}: {str(e)}")
                return False
        try:
            # Wait for the style attribute to be populated after clicking
            first_button = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.ID, f"button-{order[0]}"))
            )
            style_attr = first_button.get_attribute('style')
            if style_attr is None or style_attr == "":
                # time.sleep(1)
                # response = WebDriverWait(self.driver, 5).until(
                #     EC.presence_of_element_located((By.CLASS_NAME, "tiT-XG_word"))
                # )
                # if(response.get_dom_attribute("class") == "tiT-XG_word tiT-XG_invalidshake"):
                #     print(response.text)
                #     if(response.text=="Not a word"):
                #         print(word)
                #         self.removed.append(word)
                #         with open('removed.json', 'w') as f:
                #             json.dump(list(self.removed), f)
                #         self.bank.remove_word(word)
                #         return False
                # else:
                #     print(response.text)
                #     print(word)
                #     self.real_words.append(word)
                #     with open('real_words.json', 'w') as f:
                #         json.dump(list(self.real_words), f)
                return False
            return style_attr == 'background-color: var(--blue);' or style_attr == 'background-color: var(--yellow);'
        except Exception as e:
            print(f"Could not find or check the style of button-{order[0]}: {str(e)}")
            return False

        return False

    def close(self, wait=False):
        if wait:
            input("Enter to close...")
        self.driver.quit()
