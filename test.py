import ui
import argparse
import problem
import word_bank
import json
import requests
from pandas import *
import numpy



def main():
    # r = requests.get('https://gist.githubusercontent.com/h3xx/1976236/raw/bbabb412261386673eff521dddbe1dc815373b1d/wiki-100k.txt')
    # words = r.text.split('\n')
    # words = words[26:]
    # filtered_words = [word.lower() for word in words if len(word)>3 and word.isalpha()]
 
    # reading CSV file
    data = read_csv("unigram_freq.csv")
    
    # # converting column data to list
    # filtered_words = data['word'].tolist()
    # max_length = 6 * 8
    # wordBank = list()
    # for word in filtered_words:
    #     try:
    #         if len(word) <= max_length and len(word) > 3:
    #             wordBank.append(word)
    #     except:
    #         print(word)
        
    # with open('large_common_words.json', 'w') as f:
    #     json.dump(wordBank, f)
    bank = word_bank.WordBank("common_words.json")
    wordBankSet = bank.getWordBank()
    bank2 = word_bank.WordBank("large_common_words.json")
    wordBankSet2 = bank2.getWordBank()
    bank3 = word_bank.WordBank("word_bank.json")
    wordBankSet3 = bank3.getWordBank()
    print(len(wordBankSet))
    print(len(wordBankSet2))
    print(len(wordBankSet3))


    bank4 = word_bank.WordBank("removed.json")
    removed = bank4.getWordBank()
    bank5 = word_bank.WordBank("real_words.json")
    real_words = bank5.getWordBank()
    print(len(removed))
    print(len(real_words))

    filtered_words = data['word'].tolist()
    freq = data['count'].tolist()
    real_freq=[]
    removed_freq=[]
    i=0
    for word in real_words:
        try:
            real_freq.append(freq[filtered_words.index(word)])
        except:
            i+=1
            #print(word)
    print("removed")
    for word in removed:
        try:
            removed_freq.append(freq[filtered_words.index(word)])
        except:
            i+=1
            #print(word)
    print(getStats(real_freq))
    print(getStats(removed_freq))
    real_sorted = sorted(real_freq)
    removed_sorted = sorted(removed_freq)
    print(list(map(lambda x:round(x), numpy.quantile(real_sorted, [0,0.25,0.5,0.75,1]))))
    print(list(map(lambda x:round(x), numpy.quantile(removed_sorted, [0,0.25,0.5,0.75,1]))))
    print(real_sorted[:10])
    files=["games/strands_03_04_2024.txt",
           "games/strands_03_05_2024.txt",
           "games/strands_03_06_2024.txt",
           "games/strands_03_07_2024.txt",
           "games/strands_03_08_2024.txt",
           "games/strands_03_09_2024.txt",
           "games/strands_03_10_2024.txt",
           "games/strands_03_11_2024.txt",
           "games/strands_03_12_2024.txt",
           "games/strands_03_13_2024.txt",
           "games/strands_03_14_2024.txt",
           "games/strands_03_15_2024.txt",
           "games/strands_03_16_2024.txt",
           "games/strands_03_17_2024.txt",
           "games/strands_03_18_2024.txt",
           "games/strands_03_19_2024.txt",
           "games/strands_03_20_2024.txt",
           "games/strands_03_21_2024.txt",
           "games/strands_03_22_2024.txt",
           "games/strands_03_23_2024.txt",
           "games/strands_03_24_2024.txt",
           "games/strands_03_25_2024.txt",
           "games/strands_03_26_2024.txt",
           "games/strands_03_27_2024.txt",
           "games/strands_03_28_2024.txt",
           "games/strands_03_29_2024.txt"]
    answers = []
    for file in files:
        answers += parse_puzzle_file(file)

    answers_freq=[]
    for word in answers:
        try:
            answers_freq.append(freq[filtered_words.index(word)])
        except:
            i+=1
            print(word)

    print(getStats(answers_freq))
    print(list(map(lambda x:round(x), numpy.quantile(answers_freq, [0,.01,.02,.05,.1,.15,0.25,0.5,0.75,1]))))


def parse_puzzle_file(filename):
    with open(filename, 'r') as file:
        content = file.read()

    sections = content.split('\n\n')

    answers = [answer.lower() for answer in sections[2].replace('ANSWERS:\n', '').split('\n') if answer]

    return answers


def getStats(somelist):
    min_value = somelist[0]
    max_value = somelist[0]
    total = somelist[0]
    i=1
    for value in somelist:
        total+=value
        i+=1
        if value < min_value:
            min_value = value
        if value > max_value:
            max_value = value
    return (min_value,max_value,total/i)
        

if __name__ == "__main__":
    main()