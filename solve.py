import ui
import argparse
import problem
import word_bank

# Parse command-line arguments to get the filename and other options
def parse_args(heuristicOptions):
    parser = argparse.ArgumentParser(description="A word search problem solver.")
    parser.add_argument(
        '-f',
        '--file',
        type=str,
        default="games/strands_03_04_2024.txt",
        help="Path to the puzzle file in txt format."
    )
    parser.add_argument(
        '-o',
        '--online',
        action='store_true',
        help="If set, solves the puzzle online."
    )
    parser.add_argument(
        '-u',
        '--url',
        type=str,
        default="https://www.nytimes.com/games/strands",
        help="URL to the puzzle if solving online."
    )
    parser.add_argument(
        '--heuristic',
        type=str,
        choices=heuristicOptions,
        default='null',
        help="Heuristic used in solver."
    )
    return parser.parse_args()

def bestStarts(state,board,heuristic,wordBank):
    if(heuristic != problem.commonalityHeuristic):
        if(state == (0,0) or state == (5,0) or state == (0,7) or state == (5,7)):
            return 1
        if((state[0]==0 or state[0]==5) or (state[1]==0 or state[1]==7)):
            return 2
        return 3
    prefix = "" + board[state[1]][state[0]]
    score = 0
    num = 0
    for index,word in zip(range(len(wordBank)),wordBank):
        if(word.startswith(prefix) or word == prefix):
            score += index
            num+=1
    if(num==0):
        return float('inf')
    if(state == (0,0) or state == (5,0) or state == (0,7) or state == (5,7)):
        return (score/num)/4
    if((state[0]==0 or state[0]==5) or (state[1]==0 or state[1]==7)):
        return (score/num)/2
    return (score/num)

def main():
    heuristics = {"null":problem.nullHeuristic, "similarity":problem.similarityHeuristic, "common":problem.commonalityHeuristic}
    args = parse_args(list(heuristics.keys()))
    heuristic = heuristics[args.heuristic]

    if args.online:
        interface = ui.OnlineUserInterface(args.url, heuristic)
    else:
        interface = ui.OfflineUserInterface(args.file, heuristic)
        interface.printBoard()
    
    bank = word_bank.WordBank("large_common_words.json")
    wordBank = bank.getWordBank()
    starts = [(x, y) for y in range(8) for x in range(6)]
    sorted_starts = {start: bestStarts(start,interface.board,heuristic,wordBank) for start in starts}
    sorted_starts = dict(sorted(sorted_starts.items(), key=lambda item: item[1]))
    found_words = []
    for start in list(sorted_starts.keys()):
        word = interface.findWordsFrom(start)
        if word:
            found_words.append(word)
            print("Found words:", found_words)
    if any(any(char.isalpha() for char in string) for row in interface.board for string in row):
        redo = []
        for y, row in enumerate(interface.board):
                for x, _ in enumerate(row):
                    if not interface.board[y][x].isalpha():
                        continue
                    distance = min(abs(x - 5), abs(y - 7), x, y)
                    redo.append((distance, (x,y)))
        redo = sorted(redo, key=lambda x: x[0])
        for distance, start in redo:
            word = interface.findWordsFrom(start, False)
            if word:
                found_words.append(word)
                print("Found words:", found_words)
    if isinstance(interface, ui.OnlineUserInterface):
        interface.close(wait=False)

if __name__ == "__main__":
    main()
