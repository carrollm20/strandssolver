# Strands Solver

Strand Solver is an application designed to solve the Strands puzzle from The New York Times (w/o using hints). Users have the option to solve the current day's puzzle through online integration or the past puzzles (or puzzles from other sources) by supplying a file path to a text-based puzzle format stored locally.

## Dependencies

To install the necessary libraries, run the following command:

```bash
pip3 install -r requirements.txt
```

Ensure you have Python 3.11 or higher for optimal compatibility.

## Using Optional Arguments

The Strand Solver supports several command-line arguments to tailor the solving experience to your preferences:

- **`-f` or `--file`**: Specify the path to a puzzle file in the local system. Using this argument enables offline solving mode.

- **`-o` or `--online`**: Triggers the online solving mode to tackle the current day's puzzle on the NYT website.

- **`-u` or `--url`**: (For online mode only) You may specify a custom URL if the standard puzzle URL changes or if you wish to solve from an alternative online source. (Currently only nyt website available)

- **`--heuristic`**: Allows the user to specify a different heuristic approach defined within the code for the solver. Refer to the source code or documentation for available heuristics. (null or common)

### Solving the Current Day's Puzzle

To automatically connect to the NYT website and solve today's puzzle:

```bash
python3 solve.py -o
```

_Note: The online mode requires an active internet connection. If the puzzle contains words not present in the `large_common_words.json`, the solver might not find a complete solution._

### Solving Past Puzzles

To solve a puzzle from a specific file:

```bash
python3 solve.py -f <path_to_your_puzzle_file.txt>
```

If no file path is provided, the solver defaults to a pre-stored puzzle, `"games/strands_03_04_2024.txt"`.

```bash
python3 solve.py
```

This allows for the solving of custom or past puzzles not directly from NYT.

## Extending the Word Bank

The word bank can be extended by manually editing the `large_common_words.json` file. Add the new words in JSON array format to ensure they are recognized during future puzzle solvings.
