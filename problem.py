import ui
import heapq
import gensim

letter_frequency = {
    'e': 12.70, 't': 9.06, 'a': 8.17, 'o': 7.51, 'i': 6.97,
    'n': 6.75, 's': 6.33, 'h': 6.09, 'r': 5.99, 'd': 4.25,
    'l': 4.03, 'c': 2.78, 'u': 2.76, 'm': 2.41, 'w': 2.36,
    'f': 2.23, 'g': 2.02, 'y': 1.97, 'p': 1.93, 'b': 1.49,
    'v': 0.98, 'k': 0.77, 'j': 0.15, 'x': 0.15, 'q': 0.10, 'z': 0.07
}

class Game:
    def __init__(self, start=(0, 0), board=[['0' for x in range(6)] for y in range(8)], costFn = lambda x: 1, hint=None, answers=None, online=False, onlineUI=None, wordBank=None, exist=True, guesses=0):
        self.startState = start
        self.board = board
        self.costFn = costFn
        self.hint = hint
        self.answers = answers
        self.online = online
        self.onlineUI = onlineUI
        self.wordBank = wordBank
        self.exist = exist
        self.guesses = guesses

    def getStartState(self):
        return self.startState
    
    def getWordBank(self):
        return self.wordBank

    def getBoard(self):
        return self.board

    def isGoalState(self, actions):
        if len(actions) < 3:
            return False
        state = self.startState

        word = "" + self.board[state[1]][state[0]]
        for action in actions:
            state = (state[0] + action[0], state[1] + action[1])
            word += self.board[state[1]][state[0]]

        if not self.online and (not self.exist or self.is_word_or_singular_in_bank(word)):
            self.guesses += 1
            return word in self.answers
        if self.online and isinstance(self.onlineUI, ui.OnlineUserInterface) and (not self.exist or self.is_word_or_singular_in_bank(word)):
            self.guesses += 1
            return self.onlineUI.checkAnswer(self.startState, actions, word)
        return False

    def is_word_or_singular_in_bank(self, word):
        if word in self.wordBank:
            return True

        if word.endswith('s') and word[:-1] in self.wordBank:
            return True

        if word.endswith('es') and word[:-2] in self.wordBank:
            return True
        return False

    def getSuccessors(self, state):
        successors = []
        for dx in range (-1, 2):
            for dy in range (-1, 2):
                if dx == 0 and dy == 0:
                    continue
                nextx = state[0] + dx
                nexty = state[1] + dy
                nextState = (nextx, nexty)
                if 0 <= nextx < 6 and 0 <= nexty < 8 and self.board[nexty][nextx].isalpha() and self.checkAvailibleCorner(state, dx, dy, nextState):
                    action = (dx, dy)
                    cost = self.costFn(nextState)
                    successors.append((nextState, action, cost))
        return successors

    def checkAvailibleCorner(self, state, dx, dy, nextState):
        if abs(dx) + abs(dy) != 2:
            return True
        corner1 = self.board[state[1]][nextState[0]]
        corner2 = self.board[nextState[1]][state[0]]
        return corner1.isalpha() or corner2.isalpha() or corner1[0] != corner2[0] or abs(int(corner1[1:]) - int(corner2[1:])) != 1

class PriorityQueue:
    def __init__(self):
        self._heap = []
        self._count = 0

    def push(self, item, priority):
        entry = (priority, self._count, item)
        heapq.heappush(self._heap, entry)
        self._count += 1

    def pop(self):
        if self.is_empty():
            raise IndexError("pop from an empty priority queue")
        priority, count, item = heapq.heappop(self._heap)
        return item

    def peek(self):
        if self.is_empty():
            raise IndexError("peek from an empty priority queue")
        priority, count, item = self._heap[0]
        return item

    def is_empty(self):
        return len(self._heap) == 0

    def __len__(self):
        return len(self._heap)

def nullHeuristic(game=None, actions=None):
    return 0

def commonalityHeuristic(game,actions):
    state = game.getStartState()
    board = game.getBoard()
    hint = game.hint
    prefix = "" + board[state[1]][state[0]]
    for action in actions:
        state = (state[0] + action[0], state[1] + action[1])
        prefix += board[state[1]][state[0]]

    wordBank = game.getWordBank()
    score = 0
    num = 0
    for index,word in zip(range(len(wordBank)),wordBank):
        if(word.startswith(prefix) or word == prefix):
            score += index
            num+=1
    if(num==0):
        return float('inf')
    return score/num

    



def similarityHeuristic(game, actions):
    state = game.getStartState()
    board = game.getBoard()
    hint = game.hint
    prefix = "" + board[state[1]][state[0]]
    for action in actions:
        state = (state[0] + action[0], state[1] + action[1])
        prefix += board[state[1]][state[0]]

    model = gensim.models.Word2Vec.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)

    try:
        similarity = model.similarity(prefix.lower(), hint.lower())
    except KeyError:
        similarity = -1

    distance = 1 - similarity

    return max(distance, 0)

def aStarSearch(game: Game, wordBank, exist, heuristic=nullHeuristic):
    def couldBeAWord(actions):
        state = game.getStartState()
        board = game.getBoard()

        prefix = "" + board[state[1]][state[0]]
        for action in actions:
            state = (state[0] + action[0], state[1] + action[1])
            prefix += board[state[1]][state[0]]
        if prefix[-1] == "s":
            return any(word.startswith(prefix[:-1]) for word in wordBank)
        elif prefix[-2:] == "es":
            return any(word.startswith(prefix[:-2]) for word in wordBank)
        else:
            return any(word.startswith(prefix) for word in wordBank)

    def inWord(actions, successor):
        state = game.getStartState()
        for action in actions:
            if state == successor:
                return True
            state = (state[0] + action[0], state[1] + action[1])
        return state == successor

    problem_stack = PriorityQueue()
    problem_stack.push(item=(game.getStartState(), [], 0), priority=0)
    while not problem_stack.is_empty():
        popped = problem_stack.pop()
        if game.isGoalState(popped[1]):
            return popped[1]
        # Remove word
        for successor in game.getSuccessors(popped[0]):
            actions = popped[1] + [successor[1]]
            if inWord(popped[1], successor[0]) or (not couldBeAWord(actions) and exist):
                continue
            cost = popped[2] + successor[2]
            problem_stack.push((successor[0], actions, cost), cost + heuristic(game, actions))

    return []
