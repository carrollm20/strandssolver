import os
import re
import json

class WordBank:
    def __init__(self, file_name="word_bank.json"):
        self.file_name = file_name
        if os.path.isfile(file_name):
            self.load()
        else:
            max_length = 6 * 8
            self.wordBank = list()
            i = 1
            with open("/usr/share/dict/words", "r") as file:
                for word in file:
                    word = re.sub("[^\w]", " ", word).strip().lower()
                    if len(word) <= max_length and len(word) > 3:
                        self.wordBank.append(word)
                        i+=1
            self.save()

    def getWordBank(self):
        return self.wordBank

    def add_word(self, word):
        if word in self.wordBank:
            print(f"Word '{word}' already found in word bank.")
        else:
            self.wordBank.append(word)
            self.save()

    def remove_word(self, word):
        if word in self.wordBank:
            self.wordBank.remove(word)
            self.save()
        else:
            print(f"Word '{word}' not found in word bank.")

    def save(self):
        with open(self.file_name, 'w') as file:
            json.dump(list(self.wordBank), file)

    def load(self):
        if os.path.exists(self.file_name):
            with open(self.file_name, 'r') as file:
                self.wordBank = list(json.load(file))
